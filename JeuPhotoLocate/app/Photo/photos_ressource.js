angular.module("PhotoLocate").factory('Photo', ['$resource', function($resource){
	return $resource(window.ws+'/series/:serie_id/photos/:id', {serie_id: '@serie_id',id: '@id'},{
		update:{method:'PUT'}
	});
}]);