angular.module("PhotoLocate").factory('Partie', ['$resource', function($resource){
	return $resource(window.ws+'/partie/:id', {id: '@id'},{
		update: {method: 'PUT'}});
}])