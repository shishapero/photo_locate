angular.module("PhotoLocate").controller('PartieController',['$scope','$location','Partie','Serie','Photo',
										 function($scope, $location, Partie, Serie, Photo){
$scope.serie = Serie.query();
$scope.partie = Partie.query();
$scope.currentIndex = 0;
$scope.newgame = "";
$scope.value = "";

$scope.createGame = function(){
	var id_s = $scope.serie[(Math.floor(Math.random() * $scope.serie.length))].id;
	p = new Partie({joueur:$scope.newgame.joueur || "Anonyme", idSerie:$scope.value.id || id_s });
	p.$save(function(r){
		$scope.pseudo=p.joueur;
		$scope.score=p.score;
		$scope.isplay=true;
		setTimeout(function(){
			$scope.initMap(p.idSerie.mapRefs.split(","));
		},10); 
		$scope.photos = Photo.query({serie_id: $scope.value.id || id_s }
		);
	});
};

$scope.isCurrentSlideIndex = function (index) {
    return $scope.currentIndex === index;
};

$scope.nextSlide = function () {
	if ($scope.currentIndex !== $scope.photos.length -1){
    	$scope.currentIndex = ($scope.currentIndex < $scope.photos.length - 1) ? $scope.currentIndex + 1 : 0;
	}
	else{
		p.score=$scope.score;
		p.$update({id:p.id, score:p.score},function(){
			alert('Partie terminée, votre score : ' + $scope.score);
			$location.path('/highscore');
		});
	}
	$scope.$evalAsync();
};


$scope.initMap = function(coord) { 
    var cen= L.latLng(coord[0],coord[1]);
    var map = L.map('map', { zoomControl:false }).setView(cen, 13);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
        maxZoom: 18,
        id: 'fbergeret.onoh09oj',
        accessToken: 'pk.eyJ1IjoiZmJlcmdlcmV0IiwiYSI6ImNpamswOWw4MDAwNHF2ZWx4NDh0cXNnbnMifQ.WgFYCNEsDszDk3RNI_H7Eg'
    }).addTo(map);
    map.dragging.disable();
    map.touchZoom.disable();
	map.doubleClickZoom.disable();
	map.scrollWheelZoom.disable();
    newMarkerGroup = new L.LayerGroup();
    map.on('click', function(e){
        var coor = $scope.photos[$scope.currentIndex].position.split(",");
		var lat = coor[0];
		var lng = coor[1];
		var latlng = L.latLng(lat,lng);
		var dist = e.latlng.distanceTo(latlng);
		var coeff = p.idSerie.dist;
		var d = dist/100;
		if(d < coeff){
			$scope.score += 5;
		}
		else if(d < coeff*2){
			$scope.score += 3;
		}
		else if(d < coeff*3){
			$scope.score += 1;
		}
		else{
			$scope.score += 0;
		}
		$scope.$apply();
        var newMarker = new L.marker(e.latlng).addTo(map);
        $scope.nextSlide();
    });     
};
}]);