window.ws = 'http://localhost:8080/atelierPhotoLocate/api';
photolocate = angular.module("PhotoLocate", ['ngRoute', 'ngResource']);

photolocate.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
	$routeProvider.when('/', {templateUrl:"public/views/play.html", controller:"PartieController"});
	$routeProvider.when('/highscore', {templateUrl:"public/views/highscore.html", controller:"PartieController"});
	$routeProvider.when('/rules', {templateUrl:"public/views/rules.html", controller:""});
}]);
