angular.module("PhotoLocate").factory('Serie', ['$resource', function($resource){
	return $resource(window.ws+'/series/:id', {id: '@id'});
}])