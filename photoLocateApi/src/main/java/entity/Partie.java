/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author tetsatls
 */
@Entity
@Table(name = "partie")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Partie.findAll", query = "SELECT p FROM Partie p"),
    @NamedQuery(name = "Partie.findById", query = "SELECT p FROM Partie p WHERE p.id = :id"),
    @NamedQuery(name = "Partie.findByToken", query = "SELECT p FROM Partie p WHERE p.token = :token"),
    @NamedQuery(name = "Partie.findByNbPhotos", query = "SELECT p FROM Partie p WHERE p.nbPhotos = :nbPhotos"),
    @NamedQuery(name = "Partie.findByStatus", query = "SELECT p FROM Partie p WHERE p.status = :status"),
    @NamedQuery(name = "Partie.findByScore", query = "SELECT p FROM Partie p WHERE p.score = :score"),
    @NamedQuery(name = "Partie.findByJoueur", query = "SELECT p FROM Partie p WHERE p.joueur = :joueur")})
public class Partie implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "token")
    private String token;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nb_photos")
    private int nbPhotos;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "status")
    private String status;
    @Basic(optional = false)
    @NotNull
    @Column(name = "score")
    private int score;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "joueur")
    private String joueur;
    @JoinColumn(name = "id_serie", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Serie idSerie;

    public Partie() {
    }

    public Partie(Integer id) {
        this.id = id;
    }

    public Partie(Integer id, String token, int nbPhotos, String status, int score, String joueur) {
        this.id = id;
        this.token = token;
        this.nbPhotos = nbPhotos;
        this.status = status;
        this.score = score;
        this.joueur = joueur;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getNbPhotos() {
        return nbPhotos;
    }

    public void setNbPhotos(int nbPhotos) {
        this.nbPhotos = nbPhotos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getJoueur() {
        return joueur;
    }

    public void setJoueur(String joueur) {
        this.joueur = joueur;
    }

    public Serie getIdSerie() {
        return idSerie;
    }

    public void setIdSerie(Serie idSerie) {
        this.idSerie = idSerie;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Partie)) {
            return false;
        }
        Partie other = (Partie) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entity.Partie[ id=" + id + " ]";
    }
    
}
