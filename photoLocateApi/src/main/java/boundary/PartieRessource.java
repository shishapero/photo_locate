package boundary;

import entity.Partie;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Mourad, Etienne, Abdellah, Florian
 */

@Stateless
@Path("partie")
public class PartieRessource {
    @PersistenceContext
    EntityManager em;    
    
    //Creer une partie
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes("application/json")
    public Response addPartie(Partie part, @Context UriInfo info) throws NoSuchAlgorithmException
    {
        String username = part.getJoueur();
        String token = generationToken(username);
        part.setToken(token);
        part.setNbPhotos(10);
        part.setStatus("en cours");
        part.setScore(0);
        Partie saved = this.em.merge(part);
        return Response.ok(saved).build();
    }
    
    public String generationToken(String username) throws NoSuchAlgorithmException{
       
        String salt = "dfgt";
        String token =salt+username;
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(token.getBytes());
        byte byteData[] = md.digest();
        // convertit en String
        StringBuilder sb = new StringBuilder();
        for (int i=0; i<byteData.length; i++){
            sb.append(Integer.toString((byteData[i] & 0xff)+ 0x100, 16).substring(1));
        }
        return sb.toString();
    }
    
    //Terminer la partie
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public Response finishGame(@PathParam("id") Integer id, Partie part) {
        Partie p = this.em.find(Partie.class,id);
        p.setStatus("finie");
        p.setScore(part.getScore());
        return Response.ok(this.em.merge(p)).build();
    } 
    
    //Retourner la liste des parties
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Partie> PartieList() {
        return this.em.createNamedQuery("Partie.findAll", Partie.class)
                .getResultList();
    }   
}
