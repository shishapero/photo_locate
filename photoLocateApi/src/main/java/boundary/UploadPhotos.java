package boundary;

import entity.Photo;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;


@Path("admin/file")
public class UploadPhotos {
  @POST
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public Response uploadFichier(MultipartFormDataInput Input,@Context UriInfo info) {
      Map<String, List<InputPart>> formulaire = Input.getFormDataMap();
      List<InputPart> inputParts = formulaire.get("file");
      String current="";
      try {
          current=new java.io.File(".").getCanonicalPath();
          System.out.println("current dir"+current);
      } catch (IOException ex) {
          Logger.getLogger(UploadPhotos.class.getName()).log(Level.SEVERE, null, ex);
      }
      
      
      //List<InputPart> txt=formulaire.get("text");
      String resp="http://localhost:8080/atelierPhotoLocate/images/";
      String drect=current+"/../standalone/deployments/atelierPhotoLocate.war/images/";
      
      File directory = new File(drect);
      if(!directory.exists())
         directory.mkdir();
         
      for(InputPart ip : inputParts){
                    MultivaluedMap<String,String> headers = ip.getHeaders();
                    resp += getFileName(headers);
                    drect += getFileName(headers);
                    try{
                        InputStream is = ip.getBody(InputStream.class,null);
                        byte[] bytes = IOUtils.toByteArray(is);
                        writeFile(bytes,drect);
                        Photo ph=new Photo();
                        ph.setUrl(resp);
                        return Response.ok(ph).build();
                    }
                    catch(IOException ioe){
                        ioe.printStackTrace();
                    }
                }
      
      
      return Response.status(415).build();  
  }
  
  private void writeFile(byte[] contenu, String filename) throws IOException{
          File file = new File(filename);
          FileOutputStream fop = new FileOutputStream(file);
          
          fop.write(contenu);
          fop.flush();
          fop.close();
      
  }
  
  private String getFileName(MultivaluedMap<String, String> headers){
      String[] contenuHeader = headers.getFirst("Content-Disposition").split(";");
      
      for(String filename : contenuHeader){
          System.out.println(filename);
          if((filename.trim().startsWith("filename"))){
              System.out.println("========>>");
              String[] name = filename.split("=");
              return name[1].trim().replaceAll("\"","");
          }
      }
      return "inconnu";
  }
  public boolean deleteFile(String filePath){
          try
          {    File file = new File(filePath);
                file.delete();
                return true;
          }
          catch(Exception e)
          {
            return false;
          }
          
  }
}
