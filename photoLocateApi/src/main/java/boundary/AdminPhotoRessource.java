package boundary;

import entity.Photo;
import entity.Serie;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author KHALDI
 */
@Stateless
@Path("admin/series/{id_serie}/photos")
public class AdminPhotoRessource {
    @PersistenceContext
    EntityManager em;
    
    //Recuperer la liste des photo de la serie : id_serie
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Photo> PhotoList(@PathParam("id_serie") Integer id) {
        return this.em.createQuery("SELECT p FROM Photo p WHERE p.idSerie = :is")
                .setParameter("is", this.em.find(Serie.class,id))
                .getResultList();
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addPhoto(Photo ph,@PathParam("id_serie") Integer id_ser, @Context UriInfo info)
    {
        if(this.em.contains(this.em.find(Serie.class,id_ser))){
            ph.setIdSerie(this.em.find(Serie.class,id_ser));
            Photo saved = this.em.merge(ph);
            return Response.ok(saved).build();
        }
        return Response.status(404).entity("Serie Not Found").build();
    }
     //Modifier la serie id
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSerie(Photo photo,@Context UriInfo info)
    {
        this.em.merge(photo);
        this.em.flush();
        URI uri = info.getAbsolutePathBuilder().build();
        return Response.created(uri).build();
    }
    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSerie(@PathParam("id") Integer id,@Context UriInfo info)
    {
        
        Photo ph=this.em.find(Photo.class,id);
        UploadPhotos up=new UploadPhotos();
        
        if(up.deleteFile(ph.getUrl())){
            this.em.remove(this.em.find(Photo.class,id));
            URI uri = info.getAbsolutePathBuilder().build();
            return Response.created(uri).build();
        }
        return Response.ok().build();
    }
}
