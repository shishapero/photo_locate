package boundary;

import entity.Serie;
import java.net.URI;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author Mourad, Etienne, Abdellah, Florian
 */
@Stateless
@Path("admin/series")
public class AdminSerieRessource {
    @PersistenceContext
    EntityManager em;
    
    //Creer une serie
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response addSerie(Serie ser)
    {
        Serie saved = this.em.merge(ser);
        return Response.ok(saved).build();
    }
    
    //Retourner la liste des series
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Serie> SerieList() {
        return this.em.createNamedQuery("Serie.findAll", Serie.class)
                .getResultList();
    }
    //Retourner la serie id
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public List<Serie> SeriePhoto(@PathParam("id") Integer id) {
        return this.em.createNamedQuery("Serie.findById", Serie.class)
                .setParameter("id", id)
                .getResultList();
    }
    //Modifier la serie id
    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateSerie(Serie serie,@Context UriInfo info)
    {
        this.em.merge(serie);
        this.em.flush();
        URI uri = info.getAbsolutePathBuilder().build();
        return Response.created(uri).build();
    }
    //Supprimer la serie id
    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteSerie(@PathParam("id") Integer id,@Context UriInfo info)
    {
        this.em.remove(this.em.find(Serie.class,id));
        URI uri = info.getAbsolutePathBuilder().build();
        return Response.created(uri).build();
    }
}
