package boundary;

import entity.Photo;
import entity.Serie;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Mourad, Etienne, Abdellah, Florian
 */

@Stateless
@Path("series")
public class SerieRessource {
    @PersistenceContext
    EntityManager em;
    
    //Retourner la liste des series
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Serie> SerieList() {
        return this.em.createNamedQuery("Serie.findAll", Serie.class)
                .getResultList();
    }
    
    //Retourner la serie id
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id}")
    public List<Serie> SeriePhoto(@PathParam("id") Integer id) {
        return this.em.createNamedQuery("Serie.findById", Serie.class)
                .setParameter("id", id)
                .getResultList();
    }
    
    //Recuperer la liste des photo de la serie : id_serie
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{id_serie}/photos")
    public List<Photo> PhotoList(@PathParam("id_serie") Integer id) {
        return this.em.createQuery("SELECT p FROM Photo p WHERE p.idSerie = :id")
                .setMaxResults(10)
                .setParameter("id", this.em.find(Serie.class,id))
                .getResultList();
    }
}
