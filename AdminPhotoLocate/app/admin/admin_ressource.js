angular.module("Admin").factory('Serie', ['$resource', function($resource){
	return $resource(window.ws+'series/:id', {id: '@id'},{
		update:{method:'PUT'}
	});
}]);
angular.module("Admin").factory('Photo', ['$resource', function($resource){
	return $resource(window.ws+'series/:serie_id/photos/:id', {serie_id: '@serie_id',id: '@id'},{
		update:{method:'PUT'}
	});
}]);