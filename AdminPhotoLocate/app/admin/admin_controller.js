admin.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]);

admin.controller('adminController', ['$scope','Serie','Photo','$http', function($scope,Serie,Photo,$http){
  $scope.info="Bienvenue";
	geocoder = new google.maps.Geocoder();
	$scope.ishide="true";
	$scope.selectedItem="";
	$scope.series=Serie.query();
	$scope.ville="";
	$scope.distance=5;
	$scope.mapRefs="";
	$scope.savedS=new Serie();
	$scope.phDesc="";
	$scope.phPos="";
	$scope.islocated=false;
  $scope.edit = true;
  $scope.phTmp="";
  $scope.alert=function(msg){
    $scope.info=msg;
  };
	$scope.createSerie=function(){
		geocoder.geocode({'address': $scope.ville}, function(results, status) {
		    if (status === google.maps.GeocoderStatus.OK) {
		      $scope.mapRefs=results[0].geometry.location.lat().toString()+","+results[0].geometry.location.lng().toString();
		      var s=new Serie({dist:$scope.distance, mapRefs:$scope.mapRefs,ville:$scope.ville});
				s.$save(function(){
					$scope.series.push(s);
					$scope.ishide=false;
					$scope.savedS=s;
          $scope.photos=Photo.query({'serie_id':s.id});
					$scope.init(s.mapRefs.split(","));
          $scope.alert("Série créée avec succès, veuiller ajouter les photos");
				});
		    }
		    else
          $scope.alert("Impossible de localiser cette adresse");

		});
	}
	$scope.modifySerie=function(){
		$scope.selectedItem.$update({id:$scope.selectedItem.id});
    $scope.alert("Modification de la série est efféctuée");
	}
  $scope.$watch('selectedItem', function() {
    if($scope.selectedItem!=null)
    {
        if($scope.selectedItem.mapRefs!=undefined)
        {  
          $scope.init($scope.selectedItem.mapRefs.split(","));
          $scope.photos=Photo.query({'serie_id':$scope.selectedItem.id});
          $scope.alert("Serie "+ $scope.selectedItem.ville +" sélectionnée");
          $scope.ishide=false;
        }
    }
  });
  $scope.$watch('photos', function() {
    console.log('modif');
  });
	$scope.deleteSerie=function(){
		Serie.delete({id:$scope.selectedItem.id});
		$scope.series.splice($scope.series.indexOf($scope.selectedItem),1);
    $scope.alert("Série supprimée avec succès");
    $scope.ishide=true;
	}

	$scope.ajouterPhoto=function(){
		if($scope.islocated && $scope.phDesc!="")
		{
			var s=$scope.myFile.type.split("/");
			if(s[0]==="image")
 	        {
 	        	var uploadUrl = ws+"file";
     	        var fd = new FormData();
    	        fd.append('file', $scope.myFile);
    	        $http.post(uploadUrl, fd, {
    	            transformRequest: angular.identity,
    	            headers: {'Content-Type': undefined}
    	        })
    	        .success(function(response){
                $scope.serId = $scope.savedS.id === undefined ? $scope.selectedItem : $scope.savedS;
    	        	var ph=new Photo({description:$scope.phDesc,position:$scope.phPos,url:response.url,idSerie:$scope.serId.id});
        				ph.$save({'serie_id':$scope.serId.id},function(){
                    $scope.photos.push(ph);
                    $scope.phDesc="";
                    $scope.init($scope.serId.mapRefs.split(","));
                    $scope.alert("Photo ajoutée avec succès");
        				});
        				$scope.islocated=false;
    	        })
    	        .error(function(){
    	        	$scope.init($scope.serId.mapRefs.split(","));
                $scope.alert("Erreur lors du chargement du fichier, réssayez ulterieurement.");
    	        });
 	        }
          else {
          $scope.alert("Veuillez selectionner une Photo");
          }
		}
    else {
      if($scope.islocated)
  		{
          $scope.alert("Veuillez ajouter une description pour la photo");
      }
      else {
        $scope.alert("Veuillez localiser la photo sur la carte");
      }
    }

	}

  $scope.editPhoto = function(tof,cancel) {
      if($scope.edit)
      {
        console.log("show");
        $scope.serId = $scope.savedS.id === undefined ? $scope.selectedItem : $scope.savedS;
        $scope.alert("Vous pouvez changer la localisation de la photo en cliquant sur la carte");
        $scope.init($scope.serId.mapRefs.split(","));
        tof.edit=true;
        $scope.edit=false;
      }
      else {
        console.log("hide");
          if(cancel)
          {
            $scope.serId = $scope.savedS.id === undefined ? $scope.selectedItem : $scope.savedS;
            $scope.alert("Modification de la photo est annulée");
            $scope.photos=Photo.query({'serie_id':$scope.serId.id});

          }
          else{
              if($scope.islocated)
              {
                tof.position=$scope.phPos;
              }
              $scope.serId = $scope.savedS.id === undefined ? $scope.selectedItem : $scope.savedS;
              var ph=new Photo({description:tof.description,position:tof.position,url:tof.url,idSerie:tof.idSerie});
              Photo.update({serie_id:$scope.serId.id,id:tof.id}, ph);
              $scope.alert("Modification de la photo est enregistrée");
          }
          tof.edit=false;
          $scope.edit=true;
      }

  }
  $scope.deletePhoto=function(tof){
	   Photo.delete({'serie_id':tof.idSerie,id:tof.id});
     $scope.photos.splice($scope.photos.indexOf(tof),1);
     $scope.alert("Suppression de la photo est efféctuée");
	}


	$scope.init = function(coord) {
      $("#map").remove();
      $('<div class="col-md-6" id="map"></div>').insertAfter($("#recap"));
	    var marker = L.marker();
      $scope.islocated=false;
	    $scope.latlon;
	    var cen=L.latLng(coord[0],coord[1]);
	    // console.log(cen);
	    var map = L.map('map').setView(cen, 13);
	    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
	        maxZoom: 18,
	        id: 'fbergeret.onoh09oj',
	        accessToken: 'pk.eyJ1IjoiZmJlcmdlcmV0IiwiYSI6ImNpamswOWw4MDAwNHF2ZWx4NDh0cXNnbnMifQ.WgFYCNEsDszDk3RNI_H7Eg'
	    }).addTo(map);
	    map.on('click', function(e){
	        $scope.phPos=e.latlng.lat+","+e.latlng.lng;
	        $scope.islocated=true;
	        marker=L.marker(e.latlng).addTo(map).bindPopup($scope.phPos).openPopup();
	     });
	}
}]);
