window.ws = 'http://localhost:8080/atelierPhotoLocate/api/admin/';
admin = angular.module("Admin", ['ngRoute', 'ngResource']);

admin.config(['$routeProvider', '$httpProvider', function($routeProvider, $httpProvider){
	$routeProvider.when('/', {templateUrl:"public/views/home.html", controller:"adminController"});
	$routeProvider.when('/update', {templateUrl:"public/views/update_serie.html", controller:"adminController"});
}]);
